# Library Admin

## Overview
- Menampilkan data pinjaman buku yang diambil dari tabel student, book, dan rent.
- Menambahkan data pinjaman buku
- Delete data pinjaman buku

## How To
- Clone repo
- Buka command prompt dan arahkan cd ke lokasi clone project
- Install composer "composer install"
- Sambil menunggu composer menginstall buat database mysql di local bernama "perpustakaan"
- Untuk melakukan step ini pastikan anda sudah membuat databasenya. Untuk membuat tabel bisa kembali ke terminal codewriter dan mengetik "php artisan migrate"
- Untuk mengisi tabel simulasi dapat mengetik "php artisan db:seed"
- Copy .env dengan "cp .env.example .env"
- Generate key dengan "php artisan key:generate"
- Run aplikasi "php artisan server"
- Buka web browser dan buka http://127.0.0.1:8000/rent/list

Aplikasi siap digunakan

## Feature
Feature Website tersedia
- View library rent data
- Create library rent data
- Delete library rent data

## HowTheAppWork
- Pertama pada home terdapat daftar nama peminjam, buku peminjam, tanggal peminjaman, dan durasi peminjaman (hari).
- Apabila admin ingin menambahkan isi daftar peminjaman, dapat menekan tombol "Add Data" di bawah kiri web. Lalu akan di arahkan ke form dan admin dapat memilih buku yang dipilih peminjam dan memilih nama peminjam serta mengetik durasi lalu tekan tombol rent.
- Apabila admin ingin menghilangkan isi daftar peminjaman, dapat menekan tombol "Done" di salah satu isi yang diinginkan.

##TES
