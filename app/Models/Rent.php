<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $table = "rent";

    protected $fillable = [
        'book_id',
        'student_id',
        'duration'
      ];
}
